const fs = require("fs");
const fetch = require('node-fetch');
const writeStream = fs.createWriteStream("routes.txt");

// TODO: Get host based on environment
fetch('http://localhost:8080/wp-json/wp/v2/posts?_fields=slug')
  .then(response => response.json())
  .then((posts) => {
    posts.forEach(post => writeStream.write(`/blog/${post.slug}\n`));
    writeStream.end();
  })
  .catch(err => {
    console.log(err);
    writeStream.end();
  });

