export const environment = {
  production: true,
  host: 'https://www.86400.com.au',
  apiBase: '/wp-json/wp/v2/',
};
