import { OnInit } from '@angular/core';
import { take } from 'rxjs/operators';
import * as get from 'lodash.get';

import { Page } from '../models/page';
import { ApiService } from '../services/api/api.service';
import { TitleService } from '../services/title/title.service';
import { PageId } from './pages.constants';

export class BasePage implements OnInit {
  page: Page;
  pageId: PageId;
  pageName: string;

  constructor(
    protected apiService: ApiService,
    protected titleService: TitleService,
  ) {}

  ngOnInit() {
    if (this.pageId && !this.page) {
      this.apiService.getPage(this.pageId)
        .pipe(take(1))
        .subscribe((page) => {
          this.page = page;
          this.pageName = get(this.page, 'title.rendered');
          if (this.pageName) {
            this.setPageName(this.pageName);
          }
        });
    } else {
      this.setPageName(this.pageName);
    }

    this.prefetchPagesAndPosts();
  }

  setPageName(name: string) {
    this.titleService.setTitle(name);
  }

  private prefetchPagesAndPosts() {
    this.apiService.getPages().pipe(take(1)).subscribe();
    this.apiService.getPosts().pipe(take(1)).subscribe();
  }
}
