import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import * as get from 'lodash.get';

import { ApiService } from '../../services/api/api.service';
import { TitleService } from '../../services/title/title.service';
import { BasePage } from '../base-page';
import { Post } from '../../models/post';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent extends BasePage implements OnInit {
  post: Post;

  constructor(
    protected apiService: ApiService,
    protected titleService: TitleService,
    private activatedRoute: ActivatedRoute,
  ) {
    super(apiService, titleService);
  }

  ngOnInit() {
    super.ngOnInit();

    const slug = this.activatedRoute.snapshot.paramMap.get('slug');

    this.apiService.getPost(slug)
      .pipe(take(1))
      .subscribe((post) => {
        this.post = post;
        const postTitle = get(this.post, 'title.rendered');
        if (postTitle) {
          this.setPageName(postTitle);
        }
      });
  }
}
