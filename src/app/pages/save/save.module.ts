import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SaveRoutingModule } from './save-routing.module';
import { SaveComponent } from './save.component';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [SaveComponent],
  imports: [
    CommonModule,
    SaveRoutingModule,
    ComponentsModule,
  ]
})
export class SaveModule { }
