import { Component } from '@angular/core';

import { ApiService } from '../../services/api/api.service';
import { TitleService } from '../../services/title/title.service';
import { PageId } from '../pages.constants';
import { BasePage } from '../base-page';

@Component({
  selector: 'app-save',
  templateUrl: './save.component.html',
  styleUrls: ['./save.component.scss']
})
export class SaveComponent extends BasePage {
  pageId = PageId.Save;

  constructor(
    protected apiService: ApiService,
    protected titleService: TitleService,
  ) {
    super(apiService, titleService);
  }
}
