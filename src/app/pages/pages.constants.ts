// TODO: Make this specific for each environment (or use slug)

export enum PageId {
  EnergySwitch = 1703,
  Home = 1850,
  Pay = 1796,
  ReferAFriend = 1859,
  Save = 1798,
}
