import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';

import { ApiService } from '../../services/api/api.service';
import { TitleService } from '../../services/title/title.service';
import { BasePage } from '../base-page';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss']
})
export class BlogComponent extends BasePage implements OnInit {
  pageName = 'Blog';
  posts = [];

  constructor(
    protected apiService: ApiService,
    protected titleService: TitleService,
  ) {
    super(apiService, titleService);
  }

  ngOnInit() {
    super.ngOnInit();

    this.apiService.getPosts()
      .pipe(take(1))
      .subscribe((posts) => {
        this.posts = posts;
      });
  }
}
