import { Component } from '@angular/core';

import { ApiService } from '../../services/api/api.service';
import { TitleService } from '../../services/title/title.service';
import { PageId } from '../pages.constants';
import { BasePage } from '../base-page';

@Component({
  selector: 'app-pay',
  templateUrl: './pay.component.html',
  styleUrls: ['./pay.component.scss']
})
export class PayComponent extends BasePage {
  pageId = PageId.Pay;

  constructor(
    protected apiService: ApiService,
    protected titleService: TitleService,
  ) {
    super(apiService, titleService);
  }
}
