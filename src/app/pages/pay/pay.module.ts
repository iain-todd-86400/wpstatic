import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PayRoutingModule } from './pay-routing.module';
import { PayComponent } from './pay.component';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [PayComponent],
  imports: [
    CommonModule,
    PayRoutingModule,
    ComponentsModule,
  ]
})
export class PayModule { }
