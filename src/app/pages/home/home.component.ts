import { Component } from '@angular/core';

import { ApiService } from '../../services/api/api.service';
import { TitleService } from '../../services/title/title.service';
import { PageId } from '../pages.constants';
import { BasePage } from '../base-page';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends BasePage {
  pageId = PageId.Home;

  constructor(
    protected apiService: ApiService,
    protected titleService: TitleService,
  ) {
    super(apiService, titleService);
  }
}
