export interface Page {
  id: number;
  slug: string;
  title: PageTitle;
  content: PageContent;
  acf: PageAcf;
}

export interface PageTitle {
  rendered: string;
}

export interface PageContent {
  rendered: string;
  protected: boolean;
}

export interface PageExcerpt {
  rendered: string;
  protected: boolean;
}

export interface PageAcf {
  section: PageAcfSection[];
}

export interface PageAcfSection {
  acf_fc_layout: 'feature_block' | 'intro';
  use_animation: boolean;
  image: boolean;
  animation_shortcode: string;
  alt: string;
  header: string;
  feature_text: string;
  text_color: string;
  background_color: string;
  show_image_left_side: boolean;
  section_gtm_event: string;
  bottom_aligned_animation: boolean;
}

