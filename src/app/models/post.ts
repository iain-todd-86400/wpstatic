export interface Post {
  slug: string;
  title: PostTitle;
  content: PostContent;
}

export interface PostTitle {
  rendered: string;
}

export interface PostContent {
  rendered: string;
  protected: boolean;
}
