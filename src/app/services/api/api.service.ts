import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';

import { Page } from '../../models/page';
import { Post } from '../../models/post';
import { PageId } from '../../pages/pages.constants';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  // TODO: Use host from environments
  // private baseUrl = environment.host + environment.apiBase;
  private baseUrl = 'http://localhost:8080/' + environment.apiBase;
  private cache: { [key: string]: Observable<any> } = {};

  constructor(private http: HttpClient) {}

  getPages(): Observable<Page[]> {
    return this.callApi('pages?per_page=100', ['content', 'title', 'id', 'acf']);
  }

  getPage(id: PageId): Observable<Page> {
    return this.getPages().pipe(map(pages => pages.find(page => page.id === id)));
  }

  getPosts(): Observable<Post[]> {
    return this.callApi('posts', ['content', 'title', 'slug']);
  }

  getPost(slug: string): Observable<Post> {
    return this.getPosts().pipe(map(posts => posts.find(post => post.slug === slug)));
  }

  private callApi(endpoint: string, fields: any[] = []): Observable<any> {
    if (fields.length) {

      endpoint += `${ endpoint.includes('?') ? '&' : '?' }_fields=${fields.join(',')}`;
    }

    if (!this.cache[endpoint]) {
      this.cache[endpoint] = this.http
        .get(this.baseUrl + endpoint)
        .pipe(
          publishReplay(1),
          refCount(),
        );
    }

    return this.cache[endpoint];
  }
}
