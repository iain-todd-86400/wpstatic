import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class TitleService {
  baseTitle = '86 400 – Australia\'s first smartbank - It\'s just smart banking';

  constructor(private titleService: Title) { }

  setTitle(title: string = '') {
    const isHome = title.toLowerCase() === 'home';

    if (!title || isHome) {
      this.titleService.setTitle(this.baseTitle);
    } else {
      this.titleService.setTitle(`${title} – ${this.baseTitle}`);
    }
  }
}
