import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { LogoComponent } from './logo/logo.component';
import { MenuComponent } from './menu/menu.component';
import { PageComponent } from './page/page.component';

@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    LogoComponent,
    MenuComponent,
    PageComponent,
  ],
  imports: [
    RouterModule,
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    LogoComponent,
    MenuComponent,
    PageComponent,
  ],
})
export class ComponentsModule { }
